#!/bin/bash

ssh pi@192.168.1.50 <<EOF
sudo rm -r /var/www/api/*
sudo rm -r /var/www/home/*
sudo cp -r /var/tmp/home.com/* /var/www/home
sudo cp -r /var/tmp/release/* /var/www/api
sudo touch /var/www/api/plants.log
sudo chmod 777 /var/www/api/plants.log
sudo systemctl restart nginx
exit
EOF