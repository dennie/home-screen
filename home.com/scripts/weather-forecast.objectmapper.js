(function(objectmapper) {
	objectmapper.extractParameters = function(forecasts) {
		var items = [];
		$.each(forecasts, function(idx, item) {
			items.push({
				time: item.validTime,
				displayTime: formatting.getFormattedTime(item),
				temp: formatting.formatTemperature(item.parameters.filter(x => x.name === 't').map(parameter => parameter.values[0])[0]),
				displaysymbol: formatting.getWeatherIconCssClass(item.parameters.filter(x => x.name === 'Wsymb2').map(parameter => parameter.values[0])[0]),
				symbol: item.parameters.filter(x => x.name === 'Wsymb2').map(parameter => parameter.values[0])[0],
				precipitation: item.parameters.filter(x => x.name === 'pcat').map(parameter => parameter.values[0])[0],
				windspeed: formatting.formatWind(item.parameters.filter(x => x.name === 'ws').map(parameter => parameter.values[0])[0]),
				winddirection: item.parameters.filter(x => x.name === 'wd').map(parameter => parameter.values[0])[0],
				totalcloudcover: item.parameters.filter(x => x.name === 'tcc_mean').map(parameter => parameter.values[0])[0]
			});
		});

		return items;
	}
	objectmapper.buildHtml = function(item) {
		return `<li>
					<span class='period'>${item.displayTime}</span>
					<span class='columns'>
						<span class='column1'>
							<span class='wi wi-${item.displaysymbol}'></span>
						</span>
						<span class='column2'>
							<span class='temp'>${item.temp} <span class='wi wi-celsius'></span></span>
							<span class='wind-and-direction'>
								<span class='wi wi-direction-up' style='transform: rotate(${item.winddirection}deg)'></span>
								<span class='wind-speed'>${item.windspeed} m/s</span>
							</span>
						</span>
					</span>
				</li>`;
	}
}(window.objectmapper = window.objectmapper || {}));