(function(api) {
	function parse(response) {
		return $.parseJSON(JSON.stringify(response));
	}

	api.getForecasts = function(latidude, longitude) {

		return $.ajax({
			url: 'https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/' + longitude + '/lat/' + latidude + '/data.json',
			timeout: 5000,
			dataType: 'json'
		}).then(function(response) {
			if (response.error) {
				return $.Deferred().reject(response.error);
			}

			var result = parse(response);
			return result.timeSeries;
		});
	}
}(window.api = window.api || {}));