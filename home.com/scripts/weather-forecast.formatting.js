(function(formatting){
	formatting.getFormattedTime = function (item) {
		var now = new Date();
		var tomorrow = new Date();
		tomorrow.setDate(new Date().getDate() + 1);

		var date = new Date(item.validTime);

		var oneHour = (1000 * 60 * 60);
		var nextHours = (1000 * 60 * 60 * 12);
		var isTomorrow = (tomorrow.toDateString() == date.toDateString());

		if (date - now < oneHour){
			return "Nu";
		}

		if (date - now < nextHours){
			return "Senare";
		}

		if (isTomorrow){
			return "Imorgon";
		}

		return "Senare";
	}
	formatting.formatTemperature = function(temp){
		return  Math.round(temp);
	}
	formatting.formatWind = function(windspeed){

		return  Math.round(windspeed);
	}
	formatting.getWeatherIconCssClass = function(symbol){
		switch(symbol){
			case 1 : return "day-sunny";
			case 2 : return "day-cloudy";
			case 3 : return "day-cloudy";
			case 4 : return "day-cloudy";
			case 5 : return "cloudy";
			case 6 : return "cloudy";
			case 7 : return "fog";
			case 8 : return "showers";
			case 9 : return "showers";
			case 10 : return "rain";
			case 11 : return "thunderstorm";
			case 12 : return "sleet";
			case 13 : return "sleet";
			case 14 : return "rain-mix";
			case 15 : return "snow";
			case 16 : return "snow";
			case 17 : return "snow";
			case 18 : return "rain";
			case 19 : return "rain";
			case 20 : return "rain";
			case 21 : return "thunderstorm";
			case 22 : return "rain-mix";
			case 23 : return "rain-mix";
			case 24 : return "rain-mix";
			case 25 : return "snow";
			case 26 : return "snow";
			case 27 : return "snow";

			default : return "";
		}
	}
}(window.formatting = window.formatting || {}));