(function(filters) {
	filters.selectForecastIntervals = function(forecasts) {
		return [forecasts[0], forecasts[6], forecasts[20]];
	}
}(window.filters = window.filters || {}));