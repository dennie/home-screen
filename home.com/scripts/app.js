$(function() {
	var latitude = '59.259938';
	var longitude = '18.112320';
	var cachedForecasts = [];
	
	var recurringForecasts = function() {
		api.getForecasts(latitude, longitude).done(function(forecasts) {

			var selectedForecasts = filters.selectForecastIntervals(forecasts);
			cachedForecasts = objectmapper.extractParameters(selectedForecasts);

			$('#forecasts').empty();
			$.each(cachedForecasts, function(idx, item) {
				var listItem = objectmapper.buildHtml(item);
				$('#forecasts').append(listItem);
			});
		});
	}

	var dateAndTime = function () {
		var date = new Date()
		var day = date.getDay();

		const months = ['januari', 'februari', 'mars', 'april', 'maj', 'juni', 'juli', 'augusti', 'september', 'oktober', 'november', 'december']
		const days = ['söndag', 'måndag', 'tisdag', 'onsdag', 'torsdag', 'fredag', 'lördag']
		const colors = ['red', 'pink', 'silver', 'gold', 'pink', 'red', 'green']

		$('#month-name').html(months[date.getMonth()]);
		$('#day-name').html(days[day]);
		$('#day-name').removeClass();
		$('#day-name').addClass(colors[day])
		$('#day-number').html(date.getDate());
		$('#time').html(date.toLocaleTimeString('sv-SE', { hour: 'numeric', minute: 'numeric' }));
	}
	
	var specialDay = function () {
		// Info about this day // API: https://api.dryg.net/
		var now = new Date();

		var year = now.getFullYear();
		var month = now.getMonth()+1;
		var day = now.getDate();
		
		if (day < 10) {
		  day = '0' + day;
		}
		if (month < 10) {
		  month = '0' + month;
		}
		
		var formattedDate = year + '/' + month + '/' + day
		var calendarApiUrl = 'https://sholiday.faboul.se/dagar/v2.1/' + formattedDate;
		console.log(calendarApiUrl);

		$.ajax({
			url: calendarApiUrl, //now.toLocaleDateString('sv-SE', {year: 'numeric', month: '2-digit', day: '2-digit'}).replaceAll('-', '/'),
			dataType: 'json',
			success: function (data) {
				$('#name-of-the-day').html('vecka ' + data.dagar[0].vecka + '<br/>' + data.dagar[0].namnsdag.join(', '));
				$('#special-day').html(data.dagar[0].helgdag);
				if(data.dagar[0].flaggdag){
					$('#flag-day').css('display', 'block');
				}
			},
			error: function(err){
				console.log("specialDay Error: " + new Date() + " " + err);
			}
		});

		const secondDate = new Date(now.getFullYear(), 6, 10);
		var oneDay = 1000 * 60 * 60 * 24;
		const diffDays = Math.round(Math.abs((secondDate - now) / oneDay));
		$('#daycount').html(diffDays);
	}

	var crimes = function () {
		var crimesApiUrl = 'https://brottsplatskartan.se/api/events/?area=stockholms län';
		console.log(crimesApiUrl);

		$.ajax({
			url: crimesApiUrl,
			dataType: 'json',
			success: function (data) {
				$('#crimes').empty();
				for (i = 0; i < 2; i++) {
					$('#crimes').append('<li><span class="title">' + data.data[i].title_type + ':</span> ' + data.data[i].description + '</li>');
				  } 
			},
			error: function(err){
				console.log("crimes Error: " + new Date() + " " + JSON.stringify(err));
			}
		});
	}
	
	var selfReload = function () {
		location.reload();
		console.log("Reloaded: " + new Date())
	}

	var plants = function () {
		var plantsApiUrl = 	'http://localhost:5000/api/plants/last';
		console.log(plantsApiUrl);

		$.ajax({
			url: plantsApiUrl,
			dataType: 'json',
			success: function (data) {
				$('#alerts').empty();
				if(typeof data !== "undefined"){
					if (data.needWater){
						$('#alerts').append("<li id=\"waterplants\">Det är dags att vattna blommorna!</div>");		
						console.log("Checked for water: " + new Date() + " needWater: " + data.needWater)
					}
				}else{
					console.log("No data yet: " + data);
				}
			},
			error: function(err){
				console.log("crimes Error: " + new Date() + " " + JSON.stringify(err));
			}
		});
	}

	recurringForecasts();
	dateAndTime();
	specialDay();
	crimes();
	plants();

	setInterval(dateAndTime, 1000);
	setInterval(recurringForecasts, (1000 * 60 * 60));
	setInterval(specialDay, (1000 * 60 * 60));

	setInterval(crimes, (1000 * 60 * 1));
	setInterval(plants, (1000 * 60 * 5));

	setInterval(selfReload, (1000 * 60 * 60));

	const urlParams = new URLSearchParams(window.location.search);
	const speed = urlParams.get('speed');

	if(speed){
		document.querySelector('video').playbackRate = speed;
	}

	if(urlParams.get('novideo')){
		document.querySelector('video').remove();
	}
	if(urlParams.get('noimage')){
		$('#bg-image').remove();
	}
}(jQuery));