﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoreApi.Models;

namespace CoreApi.Data
{
    public interface IUglyDataReaderWriterThingy
    {
        Task<IEnumerable<Plant>> All();
        Task AppendLineToFileAsync(string line);
    }
}