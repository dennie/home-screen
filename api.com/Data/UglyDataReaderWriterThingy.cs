﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CoreApi.Models;
using Microsoft.AspNetCore.Hosting;

namespace CoreApi.Data
{
    public class UglyDataReaderWriterThingy : IUglyDataReaderWriterThingy
    {
        private IWebHostEnvironment hostingEnvironment;

        public UglyDataReaderWriterThingy(IWebHostEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        private string FilePath => Path.Combine(hostingEnvironment.ContentRootPath, @"plants.log");

        public async Task<IEnumerable<Plant>> All()
        {
            var list = new List<Plant>();

            if (File.Exists(FilePath))
            {
                var lines = await File.ReadAllLinesAsync(FilePath);

                foreach (var line in lines)
                {
                    var p = line.Split(",");
                    list.Add(new Plant
                    {
                        Id = int.Parse(p[0]),
                        Name = p[1],
                        NeedWater = bool.Parse(p[2]),
                        Created = DateTime.Parse(p[3])
                    });
                }
            }

            return list;
        }

        public async Task AppendLineToFileAsync(string line)
        {
            if (!File.Exists(FilePath))
            {
                File.Create(FilePath).Close();
            }

            await using (var file = File.Open(FilePath, FileMode.Append, FileAccess.Write))
            {
                await using (var writer = new StreamWriter(file))
                {
                    await writer.WriteLineAsync(line);
                    await writer.FlushAsync();
                }
            }
        }
    }
}
