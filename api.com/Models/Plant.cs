using System;

namespace CoreApi.Models{
    public class Plant{
        public int Id { get; set; }
        public string Name { get; set; }
        public bool  NeedWater { get; set; }
        public DateTime Created { get; set; }
    }
}