﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CoreApi.Models;
using System.Linq;
using System.Threading.Tasks;
using CoreApi.Data;
using Microsoft.AspNetCore.Cors;

namespace CoreApi.Controllers
{
    [ApiController]
    [Route("/api/plants")]
    [EnableCors("AllowOrigin")]  
    public class PlantsController : ControllerBase
    {
        private readonly IUglyDataReaderWriterThingy data;

        public PlantsController(IUglyDataReaderWriterThingy data)
        {
            this.data = data;
        }

        [HttpGet]
        public async Task<IEnumerable<Plant>> All()
        {
            return await data.All();
        }

        [HttpGet("last")]
        public async Task<ActionResult<Plant>> Last()
        {
            var all = await data.All();
            return all.LastOrDefault();
        }

        [HttpPost]
        public async Task Add(Plant plant)
        { 
            await data.AppendLineToFileAsync($"{plant.Id},{plant.Name},{plant.NeedWater},{DateTime.Now}");
        }

        
    }
}
