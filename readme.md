# Screen | .NET Core API | Python app

Videos: https://www.videvo.net/video/bokeh-light-rays/214/

> home.com Screen 			 	
> ```192.168.1.50  /var/www/home``` 

> api.com .NET Core API
> ```192.168.1.50  /var/www/api```

> Plants Python app
> ```192.168.1.51  ~/projects/home```

![preview](preview.png) 

## Startup

192.168.1.50
$> ```chromium-browser -kiosk http://localhost```
$> ```/var/www/api dotnet CoreApi.dll```

192.168.1.51
$>```~/projects/plants/ python plants.py```

### Deploy

FTP -> 192.168.1.50 /var/temp/home
$>```sudo cp -r /var/tmp/home/* /var/www/home```

FTP -> 192.168.1.50 /var/temp/api
$>```sudo cp -r /var/tmp/api/* /var/www/api```

### Access rights

$>```sudo touch plants.log```
$>```sudo chmod 777 plants.log```

### Install log

#### Port forwarding

Ext. start  Ext. end    Intern start    Intern end     Forward to
5000        5001        5000            5001           192.168.1.50

#### Install .NET Core

$> ```curl -sSL https://dot.net/v1/dotnet-install.sh | bash /dev/stdin```
$> ```echo 'export DOTNET_ROOT=$HOME/.dotnet' >> ~/.bashrc```
$> ```echo 'export PATH=$PATH:$HOME/.dotnet' >> ~/.bashrc```
$> ```source ~/.bashrc```

#### Static IP

$>```sudo nano /etc/dhcpcd.conf```
```
interface eth0
static ip_address=192.168.1.50/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1
```
#### Nginx 

$>```sudo systemctl restart nginx```
$>```sudo nano /etc/nginx/sites-enabled/default```
```
server {
        listen 80;
        server_name api.com;
        root /var/www/api;

        location / {
                proxy_pass http://localhost:5000;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection keep-alive;
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;

                add_header 'Access-Control-Allow-Origin' '*' always;
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
                add_header 'Access-Control-Allow-Headers' 'Origin, X-Requested-With, Content-Type, Accept' always;
                add_header 'Access-Control-Allow-Credentials' 'true' always;
        }
}
server {
        listen 80;
        listen [::]:80;
        server_name home.com;
        root /var/www/home;
        index index.html;

        location / {
                try_files $uri $uri/ = 404;
        }
}
```

## App: plants   192.168.1.51

> Raspberry Pi - Soil Moist Sensor - Python script post to .NET API

$>```sudo nano ~/projects/plants/plants.py```

```
import RPi.GPIO as GPIO
from time import sleep
import requests
import json

GPIO.setmode(GPIO.BCM)
DEBUG = 1
LOGGER = 1

GPIO.setup(21, GPIO.IN)
headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

while True:
    needWater = (GPIO.input(21) == 0)
    data = {'Id':1, 'Name': 'Lila palett', 'NeedWater': needWater }
    req = requests.post(url ='http://api.com/api/Plants', json=data, headers=headers)
    print data
    print req
    sleep(6*3600); # 6*1 hour
```

### Install log

#### Routing hosts file

$>```sudo nano /etc/hosts```
192.168.1.50	home.com
192.168.1.50	api.com

#### Static IP

$>```sudo nano /etc/dhcpcd.conf```
```
interface eth0
static ip_address=192.168.1.51/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1
```
## TODOS

* Busstrafik
* Födelsedagar
* Nästa DIF match / Tabellplacering
* Delad kalender
* Nästa skidevent

## Combination of SMHI forecast API and weathericons.io

https://opendata.smhi.se/apidocs/metfcst/parameters.html
https://erikflowers.github.io/weather-icons/

